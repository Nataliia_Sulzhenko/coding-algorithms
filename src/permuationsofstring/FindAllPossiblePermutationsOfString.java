package permuationsofstring;

import java.util.ArrayList;
import java.util.List;

/**
 * FindAllPossiblePermutationsOfString.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class FindAllPossiblePermutationsOfString {

    /**
     * Find all permutations.
     *
     * @param str the str
     * @return the string [ ]
     */
    public List<String> findAllPermutations(String str){
        List<String> result = new ArrayList<String>();
       return  result;
    }
    /**
     * Find all permutations.
     *
     */
    static void permuteString(){
        String str= "abc";
        str.codePointAt(0);
        char[] chars = str.toCharArray();
        permute(chars, str);
    }

//a,b,c; a,c,b; b,a,c; b,c,a; c,a,b, c,b,a
    private static void permute(char[] chars, String inputStr) {
        for (int i = 0; i < chars.length; i++) {
            int j = i + 1;
            if (j < chars.length) {
                char temp = chars[i];
                chars[i] = chars[j];
                chars[j] = temp;
                for (char k : chars) {
                    System.out.println(k);
                }
                System.out.println("============");
            }

        }
        if (!inputStr.equals(String.valueOf(chars))){
            permute(chars, inputStr);
        }
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String [] args) {
        permuteString();
    }
}
