package multithread;

/**
 * PinPong.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class PinPong {

    /**
     * Instantiates a new Pin pong.
     */
    public PinPong() {
    }


     static class Game implements Runnable {
         private static final String PING = "ping";
         private static final String PONG = "pong";
         private volatile String previousValue;
         private Object monitor = new Object();
         private boolean simofor = false;

        @Override
        public void run() {
            while (true) {
                synchronized (monitor){
                    if (simofor) {

                       previousValue = PING.equals(previousValue) ? PONG : PING;

                        System.out.println("thread : " + Thread.currentThread().getName() + " " +  previousValue);

                        simofor = false;
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            //
                        }
                    } else {
                        simofor = true;
                        monitor.notifyAll();
                    }
                }
            }
        }
    }


    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Game pong = new Game();

        Thread t1 = new Thread(pong, "thread1");
        Thread t2 = new Thread(pong, "thread2");

        t1.start();
        t2.start();

    }


}
