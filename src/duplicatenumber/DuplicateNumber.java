package duplicatenumber;


/**
 * DuplicateNumber.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class DuplicateNumber {
    /**
     * Find duplicate number.
     *
     * @param numbers the numbers
     * @return the int
     */
    public int findDuplicateNumber(int[] numbers){

        int highestNumber = numbers.length-1;
        int total = getSum(numbers);
        int duplicate = total - (highestNumber*(highestNumber+1)/2);
        return duplicate;
    }

    /**
     * Get sum.
     *
     * @param numbers the numbers
     * @return the int
     */
    public int getSum(int[] numbers){

        int sum = 0;
        for(int num:numbers){
            sum += num;
        }
        return sum;
    }

    /**
     * Main void.
     *
     * @param a the a
     */
    public static void main(String[] a){
        int[] numbers = new int[11];
        for(int i=0; i<10; i++){
            numbers[i] = i+1;
        }
        //add duplicate number into the list
        numbers[10] = 6;
        DuplicateNumber dn = new DuplicateNumber();
        System.out.println("Duplicate Number: "+dn.findDuplicateNumber(numbers));
    }
}
