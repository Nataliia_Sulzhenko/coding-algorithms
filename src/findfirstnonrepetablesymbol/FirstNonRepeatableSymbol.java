package findfirstnonrepetablesymbol;

import java.util.HashMap;
import java.util.Map;

/**
 * FirstNonRepeatableSymbol.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class FirstNonRepeatableSymbol {


    /**
     * Find first non repeatable symbol.
     *
     * @param str the str
     * @return the char
     */
    public Character findFirstNonRepeatableSymbol(String str) {
        char[] chars = str.toCharArray();

        Map<Character, Integer> countMap = new HashMap<Character, Integer>();

        for (char ch:chars) {
            Integer count = countMap.get(ch) == null ? 1 : countMap.get(ch);
            countMap.put(ch, count++);
        }

        for (Character ch : countMap.keySet()) {
            if (countMap.get(ch) == 1) {
                return ch;
            }
        }
        return null;
    }

    /**
     * Main void.
     *
     * @param arg the arg
     */
    public static void main(String[] arg) {
        String str = "success";
        FirstNonRepeatableSymbol fnrs = new FirstNonRepeatableSymbol();
        Character ch = fnrs.findFirstNonRepeatableSymbol(str);
        System.out.print(ch);

    }

}
