package findmiddleindex;

/**
 * You are given an array of numbers. Find out the array index or position
 *where sum of numbers preceeding the index is equals to sum of numbers
 *succeeding the index.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class FindMiddleIndex {


    /**
     * Find middle index.
     *
     * @param numbers the numbers
     * @return the int
     */
    public int findMiddleIndex(int [] numbers) {
        int sumLeft = 0;
        int totalSum = 0;

        for (int i = 0; i < numbers.length; i++) {
            totalSum += numbers[i];
        }

        int sumRight = totalSum;
        for (int i = 0; i < numbers.length; i++) {
            int ind = i;

            if (i != 0) {
                sumLeft += numbers[--ind];
            }
            sumRight -= numbers[i];
            if (sumLeft == sumRight) {
                return i;
            }
        }
        return -1;
    }


    /**
     * Main void.
     *
     * @param a the a
     */
    public static void main(String[] a){
        int[] numbers = {1,2,3,4,5,0,0,1,9};
        FindMiddleIndex fmi = new FindMiddleIndex();
        int index = fmi.findMiddleIndex(numbers);
        System.out.print(index);
    }

}
