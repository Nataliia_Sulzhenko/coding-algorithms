package reversenumber;

/**
 * ReverseNumber.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class ReverseNumber {

    /**
     * Reverse number.
     *
     * @param number the number
     * @return the int
     */
    public int reverseNumber(int number) {
        int total = 0;
        while (number>0) {
            total = total *10 + number % 10;
            number = number /10;
        }
        return total;
    }

    /**
     * The entry point of application.
     *
     * @param arg the input arguments
     */
    public static void main(String [] arg) {
        ReverseNumber rn = new ReverseNumber();
        int number = rn.reverseNumber(123456);

        System.out.print(number);

    }

}
