package mergesortedarrays;

/**
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class MergeSortedArray {

    /**
     * Merge int [ ].
     *
     * @param a the a
     * @param b the b
     * @return the int [ ]
     */
    public int[] merge(int[] a, int[] b) {
        int[] result = new int[a.length + b.length];
        int aInd = 0;
        int bInd = 0;
        int resultInd = 0;

        while (aInd < a.length && bInd < b.length) {
            if (a[aInd] < b[bInd]) {
                result[resultInd] = a[aInd++];
            } else {
                result[resultInd] = b[bInd++];
            }
            resultInd ++;
        }

        while (aInd < a.length) {
            result[resultInd++] = a[aInd++];
        }

        while (bInd < b.length) {
            result[resultInd++] = b[bInd++];
        }

        return result;
    }

    /**
     * The entry point of application.
     *
     * @param arg the input arguments
     */
    public static void main(String [] arg) {
        int [] a = {1,2,3,4,5,6};
        int [] b = {7,8,9,10,11,12};

        MergeSortedArray msa = new MergeSortedArray();
        int[] result = msa.merge(a, b);

        System.out.print(result);



    }
}
