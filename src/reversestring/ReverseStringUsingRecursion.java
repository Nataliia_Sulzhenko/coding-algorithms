package reversestring;

/**
 * ReverseStringUsingRecursion.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class ReverseStringUsingRecursion {

    /**
     * Reverse string.
     *
     * @param string the string
     * @return the string
     */
    public String reverseString(String string) {
        if (string.length() == 1) {
            return string;
        }
        int index = string.length() - 1;
        return string.charAt(index) + reverseString(string.substring(0, index));
    }

    /**
     * Reverse string simple.
     *
     * @param string the string
     * @return the string
     */
    public String reverseStringSimple(String string) {
        String total = "";
        for (char c : string.toCharArray()) {
            total = c + total;
        }

        return total;
    }

    /**
     * The entry point of application.
     *
     * @param arg the input arguments
     */
    public static void main(String [] arg) {
        ReverseStringUsingRecursion rsvr = new ReverseStringUsingRecursion();

        String string = "123456";
        String result = rsvr.reverseString(string);

        System.out.print(result);

        String simpleResult = rsvr.reverseStringSimple(string);

        System.out.print(simpleResult);
    }

}
