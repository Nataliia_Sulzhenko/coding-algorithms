package validparentheses;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * ValidParentheses.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class ValidParentheses {
    private Map<Character, Character> parenthesesMap = new HashMap<Character, Character>();

    {
        parenthesesMap.put('(', ')');
        parenthesesMap.put('[', ']');
        parenthesesMap.put('{', '}');

    }

    /**
     * Check parentheses.
     *
     * @param source the source
     * @return the boolean
     */
    public boolean checkParentheses(String source){
        char[] arr = source.toCharArray();
        Stack<Character> stack = new Stack<Character>();
        for(char c : arr) {
            if (parenthesesMap.containsKey(c)) {
                stack.push(c);
            }else if (parenthesesMap.containsValue(c)) {
                if (!stack.isEmpty() && parenthesesMap.get(stack.peek()) == c) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    /**
     * The entry point of application.
     *
     * @param agr the input arguments
     */
    public static void main(String[] agr) {
        String str = "([])()()()()()()()()";

        ValidParentheses vp = new ValidParentheses();
        boolean result = vp.checkParentheses(str);
        System.out.print(result);
    }

}
