package dynamic_programming;

import java.util.HashMap;
import java.util.Map;

/**
 * A child is running up a staircase with n steps, and can hop either 1 step, 2 steps, or
 * 3 steps at a time. Implement a method to count how many possible ways the child
 * can run up the stairs.
 * <p/>
 * Author Nataliia_Sulzhenko.
 */
public class PossibleWaysCounting {

    private Cash cash = new Cash();

    {
        //test cash
        cash.putStairsAndWaysNumber(1,1);
        cash.putStairsAndWaysNumber(2,2);
        cash.putStairsAndWaysNumber(3,4);
        cash.putStairsAndWaysNumber(4,7);
        cash.putStairsAndWaysNumber(5,13);
        cash.putStairsAndWaysNumber(6,24);
        cash.putStairsAndWaysNumber(7,44);
        cash.putStairsAndWaysNumber(8,81);
        cash.putStairsAndWaysNumber(9,149);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        PossibleWaysCounting counter = new PossibleWaysCounting();

        int stairsCount = 5900;

        int result = counter.countWaysCash(stairsCount);
        int result2 = counter.countWays(stairsCount);

        System.out.println(result);
        System.out.println(result2);
    }


    private int countWaysCash(int numberStairs) {
        if (numberStairs < 0) {
            return 0;
        } else if (numberStairs == 0) {
            return 1;
        } else if (cash.checkIfExists(numberStairs)) {
            return cash.getWaysCountByStairsNumber(numberStairs);
        } else {
            int waysNumber = countWaysCash(numberStairs - 1) +
                    countWaysCash(numberStairs - 2) +
                    countWaysCash(numberStairs - 3);
            cash.putStairsAndWaysNumber(numberStairs, waysNumber);
        }
        return cash.getWaysCountByStairsNumber(numberStairs);
    }

    private int countWays(int numberStairs) {
        if (numberStairs < 0) {
            return 0;
        } else if (numberStairs == 0) {
            return 1;
        } else {
            return countWays(numberStairs - 1) +
                    countWays(numberStairs - 2) +
                    countWays(numberStairs - 3);
        }
    }

    class Cash {
        private Map<Integer, Integer> accumulativeCash = new HashMap<>();

        /**
         * Put stairs and ways number.
         *
         * @param stairsNumber the stairs number
         * @param waysCount    the ways count
         */
        public void putStairsAndWaysNumber(int stairsNumber, int waysCount) {
            this.accumulativeCash.put(stairsNumber, waysCount);
        }

        /**
         * Get ways count by stairs number.
         *
         * @param stairsNumber the stairs number
         * @return the int
         */
        public int getWaysCountByStairsNumber(int stairsNumber) {
            Integer ways = this.accumulativeCash.get(stairsNumber);
            if (ways == null) {
                return -1;
            }
            return ways;
        }

        /**
         * Check if exists.
         *
         * @param stairsNumber the stairs number
         * @return the boolean
         */
        public boolean checkIfExists(int stairsNumber) {
            return this.accumulativeCash.get(stairsNumber) == null ? false : true;
        }
    }


}
