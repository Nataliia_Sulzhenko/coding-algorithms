package quicksort;

import org.junit.Test;

/**
 * Author Nataliia_Vasylieva.
 */
public class QuickSort {

    /**
     * Partition int.
     *
     * @param arr the arr
     * @param left the left
     * @param right the right
     * @return the int
     */
    void partition(int[] arr, int left, int right) {
        int i = left;
        int j = right;
        int index = (left + (right - left)) / 2;
        int pivot = arr[index];
        while (true) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i < j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;j--;
            }
        }
//        System.out.print("fail");
//        partition(arr, left, right);
//        partition(arr, left, right);
    }

//    /**
//     * Sort void.
//     *
//     * @param arr the arr
//     * @param left the left
//     * @param right the right
//     */
//    void sort(int [] arr, int left, int right) {
//        if (left>=right) {
//            return;
//        }
//        partition(arr, left, right);
//        sort(arr, left, index - 1);
//        sort(arr, index, right);
//
//    }

    /**
     * Test sort algorithm.
     */
    @Test
    public void testSortAlgorithm(){
        int [] arr = {9,3,7,6,5,1,2,8,4,7,3,5};
        partition(arr, 0, arr.length-1);
        for (int i=0; i< arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
//    4,5,6,7,8,9,4,3,2,1

}
