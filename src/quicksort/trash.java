package quicksort;

import org.junit.Test;

/**
 * Author Nataliia_Vasylieva.
 */
public class trash {


    /**
     * Partition int.
     *
     * @param arr the arr
     * @param left the left
     * @param right the right
     * @return the int
     */
    int partition(int arr[], int left, int right)
    {
        int i = left, j = right;
        int tmp;
        int pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (arr[i] < pivot)
                i++;
            while (arr[j] > pivot)
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        };

        return i;
    }

    /**
     * Quick sort.
     *
     * @param arr the arr
     * @param left the left
     * @param right the right
     */
    void quickSort(int arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1)
            quickSort(arr, left, index - 1);
        if (index < right)
            quickSort(arr, index, right);
    }

    /**
     * Check void.
     */
    @Test
    public void check() {
        int [] arr = {9,3,7,6,5,1,2,8,4,7,3,5};
        quickSort(arr, 0, arr.length);
    }


}
