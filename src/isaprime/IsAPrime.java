package isaprime;

/**
 * Is a prime number.
 * <p/>
 * <p/>
 * Copyright (C) 2013 copyright.com
 * <p/>
 *
 * @author Nataliia_Vasylieva
 */
public class IsAPrime {

    /**
     * Is prime.
     *
     * @param number the number
     * @return the boolean
     */
    public boolean isPrime(int number) {
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) {
                // number is perfectly divisible - no prime
                return false;
            }
        }
        return true;
    }
}
